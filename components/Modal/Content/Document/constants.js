export const questionInfoInit = index => {
    return {
        index: index,
        question: '',
        answers: [
            {
                answerIndex: '0',
                answerValue: '',
                answerResult: false
            },
            {
                answerIndex: '1',
                answerValue: '',
                answerResult: false
            },
        ]
    }
}

export default questionInfoInit