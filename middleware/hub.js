import { Hub } from 'aws-amplify';

export default function ({ store }) {
    Hub.listen('auth', () => {
        return store.dispatch('user/currentUser')
    })
}