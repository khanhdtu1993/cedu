export default function ({ store, redirect }) {
    if (!store.state.user.attributes) {
        return redirect('/login')
    }
}