
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icons/favicon.ico' },
    ],
    script: [
      { src: 'https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/styles/general.scss',
    '~/assets/styles/tooltip.scss',
    '~/assets/styles/media.scss',
    '~/assets/styles/flex.scss',
    '~/assets/styles/static.scss',
    '~/assets/styles/tabs.scss',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/amplify',
    '~/plugins/tooltip',
    // '~/static/notification/subscription',
    // '~/plugins/service-worker',
    '~/plugins/click-outside',
    '~/plugins/lodash',
    '~/plugins/tabs',
    '~/plugins/datetime',
    {
      src: '~/plugins/calendar',
      mode: 'client'
    }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  dev: (process.env.NODE_ENV !== 'production')
}
