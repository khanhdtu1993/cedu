import Vue from 'vue'

Vue.prototype.$clone = function(value) {
    return JSON.parse(JSON.stringify(value))
}