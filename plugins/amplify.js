import Vue from 'vue'
import Amplify from 'aws-amplify'
import aws_exports from '~/aws-exports'

Vue.use(Amplify.configure(aws_exports))
