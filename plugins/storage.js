export const setItem = (itemName, data) => {
    return localStorage.setItem(itemName, JSON.stringify(data))
}

export const getItem = itemName => {
    const item = JSON.parse(localStorage.getItem(itemName))
    if (item) return item
    return null
}

export const removeItem = itemName => {
    return localStorage.removeItem(itemName)
}

export const updateProperty = (itemName, key, value) => {
    const item = JSON.parse(localStorage.getItem(itemName))
    item[key] = value
    return localStorage.setItem(itemName, JSON.stringify(item))
}