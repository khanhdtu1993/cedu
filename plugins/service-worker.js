import Vue from 'vue'
import { ServiceWorker } from 'aws-amplify'

const messagingID = 'BFsY4fDT-wnCkO64FuV1XXGOE-1wFn1riHz3k_jA4dGkf8jra-2I-Nl_x60tMQH3K2X62t3IGcgIpfDmbh26sX0'

const serviceWorker = new ServiceWorker()

Vue.prototype.$push = {
  init: async function() {
    try {
      await serviceWorker.register('/notification/registration.js', '/notification/')
      await serviceWorker.enablePush(messagingID)
      console.log('Push notification is ready !')
      localStorage.setItem('Notification', 'Accepted')
    } catch (error) {
      console.log('User was denied Notification !')
      localStorage.setItem('Notification', 'Denied')
    }
  }
}
