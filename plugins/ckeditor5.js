import Vue from 'vue'
import CKEDITOR from '@ckeditor/ckeditor5-vue'

Vue.use(CKEDITOR)
