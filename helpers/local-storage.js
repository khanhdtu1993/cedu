export const setItem = (itemName, data) => {
    return localStorage.setItem(itemName, JSON.stringify(data))
}

export const getItem = itemName => {
    const item = JSON.parse(localStorage.getItem(itemName))
    if (item) return item
    return null
}

export const removeItem = itemName => {
    return localStorage.removeItem(itemName)
}

export const updateProperty = (itemName, key, value) => {
    const item = JSON.parse(localStorage.getItem(itemName))
    item[key] = value
    return localStorage.setItem(itemName, JSON.stringify(item))
}

export const userIdentities = prop => {
    const user = JSON.parse(localStorage.getItem('User'))
    const user_identites = JSON.parse(user.identities)
    if (!prop) return user_identites[0]
    return user_identites[0][prop]
}