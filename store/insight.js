import { roomCreatingStateInitial } from './index';
// initial state
const state = () => ({
    // timeline
    list: {
        data: [],
        totalPage: 0,
        currentPage: 0,
        isLoading: false,
        error: null
    },
    creating: {
        post: {
            value: '',
            images: [],
            files: []
        },
        task: {
            status: 'INIT_CREATE',
            type: null,
            data: null
        },
        poll: null
    },
    joining: {
        room_id: null
    }
})

// getters
const getters = {

}

// actions
const actions = {
    join({ state, commit }, data) {
        state.joining.room_id = data
        // check user was existed ?
        this.$router.push('/insight')
    },
    updateCreateTask() {

    },
    updatePostValue({ state, commit }, value) {
        commit('updatePostValue', value)
    }
}

// mutations
const mutations = {
    updatePostValue(state, data) {
        state.creating.post.value = data
    },
    createPost(state) {
        const type = 'POST'
        const data = state.creating.post
        const userCreated = 'KHANH'
        const datetimeCreated = new Date().getTime()
        state.list.data.push({ type, data, userCreated, datetimeCreated })
    },
    updateCreateTask(state, data) {
        state.creating.task = {
            ...state.creating.task,
            ...data
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
