import { documentCreatingStateInitial } from './index'
import { userIdentities } from '~/helpers/local-storage'
// STATE
const state = () => ({
    list: {
        data: [],
        totalPage: 0,
        currentPage: 0,
        isLoading: false,
        error: null
    },
    creating: JSON.parse(JSON.stringify(documentCreatingStateInitial)),
    playing: {
        status: 'INIT',
        isOpened: false,
        data: null,
        questionIndex: 0,
        result: {}
    },
    cke: {
        isOpened: false,
        keyValue: null,
        value: null,
    }
})

// GETTERS
const getters = {

}

// ACTIONS
const actions = {
    updateDraftAnswers({ state }, data) {
        const index = parseInt(data.answerIndex)
        const draft = state.creating.draft
        draft.answers[index].answerValue = data.answerValue
        draft.answers[index].answerValueIsChanged = data.answerValueIsChanged
        draft.answers[index].answerValueIsValid = data.answerValueIsValid
        // Auto add new a init question
        if (draft.answers[draft.answers.length - 1].answerValue) {
            draft.answers.splice(draft.answers.length - 1, 1)

            draft.answers.push({
                answerIndex: draft.answers.length + '',
                answerValue: data.answerValue,
                answerResult: false,
            })

            draft.answers.push({
                answerIndex: '-',
                answerValue: '',
                unknown: '',
            })
        // Auto remove a oldest question
        } else {
            if (index > 2 && !draft.answers[draft.answers.length - 2].answerValue) {
                draft.answers.splice(draft.answers.length - 2, 1)
            }
        }
    },
    updateDraftQuestion({ state, commit }, question) {
        commit('updateDraftQuestion', question)
    },
    updateDraftResult({ state, commit }, data) {
        const draft = state.creating.draft
        const index = parseInt(data.answerIndex)
        draft.answers[index].answerResult = data.answerResult
        // Update answer_formated again
        draft.result = ''
        draft.answers.forEach(answer => {
            if (answer.answerIndex != '-' && answer.answerResult) {
                draft.result += answer.answerIndex
            }
        })
        commit('updateDraftResult', {
            answerIndex: index,
            resultValue: draft.result,
            checkboxValue: data.answerResult
        })
    },
    resetDraft({ state }) {
        state.creating.draft.question = ''
        state.creating.draft.result = ''
        state.creating.draft.answers.forEach(answer => {
            if (answer.answerIndex !== '-') {
                answer.answerValue = ''
                answer.answerResult = false
                answer.answerValueIsChanged = false
                answer.answerValueIsValid = false
            }
        })
    },
    addDraftToList({ state, commit }) {
        let draft = state.creating.draft
        // Get answers from draft
        draft.answers_formatted = ''
        draft.answers.forEach(answer => {
            if (answer.answerIndex !== '-') {
                draft.answers_formatted += `${answer.answerIndex}-${answer.answerValue}[-]`
            }
        })

        commit('addToCreatingResult', draft.result)
        commit('addToCreatingList', draft)
        commit('updateCreatingCurrentIndex')
        commit('resetDraft')
    },
    updateListCreatingIndex({ state, commit }, index) {
        state.creating.currentIndex = index
        state.creating.draft = state.creating.list[index]
    },
    // updateCreatingScore({ state, commit }, score) {
    //     commit('updateCreatingScore', score)
    // },
    // updateCreatingTitle({ state, commit }, title) {
    //     commit('updateCreatingTitle', title)
    // },
    updateCreatingStatus({ state, commit }, status) {
        // state.creating.status = status
        if (status === 'CREATE') {
            // Init a new draft
            state.creating.draft = {
                question: '',
                answers: [
                    {
                        answerIndex: '0',
                        answerValue: '',
                        answerResult: false
                    },
                    {
                        answerIndex: '1',
                        answerValue: '',
                        answerResult: false
                    },
                    {
                        answerIndex: '-',
                        answerValue: '',
                        unknown: ''
                    }
                ],
                answers_formatted: '0-[-]1-[-]',
                result: ''
            }
            // Update currentIndex
            state.creating.currentIndex = state.creating.list.length
        }
        commit('updateCreatingStatus', status)
    },
    deleteOneCreating({ state }) {
        const index = state.creating.currentIndex
        state.creating.list.splice(index, 1)
        const latestIndex = state.creating.list.length - 1
        state.creating.currentIndex = latestIndex
        state.creating.draft = state.creating.list[latestIndex]
        // Update creating result
    },
    resetCreating({ state, commit }) {
        commit('resetCreating')
    },
    createDocument({ state, commit }) {
        let id = state.creating.id
        let title = state.creating.title
        let score = state.creating.score
        let answers = state.creating.list
        let result = state.creating.result
        let dt_created = new Date().getTime()
        let u_created = userIdentities('userId')
        // commit('createDocument', { document, result })
        if (state.creating.status !== 'CONFIRM_CREATE') {
            commit('updateCreatingStatus', 'CONFIRM_CREATE')
            return
        }
        if (state.creating.status === 'CONFIRM_CREATE') {
            answers.forEach(item => {
                delete item.answers
                delete item.result
            })
            const document = { id, title, u_created, answers, dt_created }
            commit('updateCreatingStatus', 'CONFIRMED_CREATE')
            commit('createDocument', { document, result })
        }
    },
    // addAnswerDraft({ state, commit }, { answerIndex }) {
    //     commit('onAddAnswerDraft', answerIndex)
    // },
    // addAnswerCreating({ state, commit }, question) {
    //     commit('onAddAnswerCreating')
    // }
    updateCKEValue({ state, commit }, value) {
        commit('updateCKEValue', value)
        let answerIndex = state.cke.keyValue
        if (!answerIndex || answerIndex === '-') return
        if (answerIndex === 'question') {
            commit('updateDraftQuestion', value)
        } else {
            answerIndex = parseInt(answerIndex)
            commit('updateDraftAnswer', { answerIndex, value })
        }
    },
    updateCKEKeyValue({ state, commit }, keyValue) {
        if (keyValue === '-') {
            commit('updateCKEKeyValue', null)
            commit('updateCKEValue', null)
            return
        }
        if (keyValue === 'HEADER') {
            commit('updateCKEKeyValue', 'question')
            commit('updateCKEValue', state.creating.draft.question)
            return
        }
        const value = state.creating.draft.answers[parseInt(keyValue)].answerValue
        commit('updateCKEKeyValue', keyValue)
        commit('updateCKEValue', value)
    },
    play({ state, commit }, documentId) {
        console.log(documentId)
        const document = state.list.data.find(document => {
            return document.id.toString() === documentId
        })
        console.log('Document for playing', document)
        commit('openPlaying')
        commit('formatDocumentForPlaying', { document })
    },
    closePlaying({ state, commit }) {
        commit('closePlaying')
    },
    confirmPlaying({ state, commit }) {
        if (state.playing.status === 'INIT' ) {
            commit('confirmedPlaying')
            return
        }
        if (state.playing.status === 'CONFIRMED' ) {
            commit('completedPlaying')
            return
        }
    },
    updatePlayingResult({ state, commit }, answerIndex) {
        let result = ''
        const questionIndex = state.playing.questionIndex
        state.playing.data.answers[questionIndex].answers.map(item => {
            if (item.answerIndex === answerIndex) {
                item.answerResult = !item.answerResult
                if (item.answerResult) {
                    result += answerIndex
                }
            } else {
                if (item.answerResult) {
                    result += item.answerIndex
                }
            }
        })
        console.log('COMMIT', result)
        commit('updatePlayingResult', { questionIndex, result })
    },
    updatePlayingMarker({ state, commit }) {
        const questionIndex = state.playing.questionIndex
        const question = state.playing.data.answers[questionIndex]
        const marked = question.isMarked ? false : true
        commit('updatePlayingMarker', { questionIndex, marked })
    },
}

// MUTATIONS
const mutations = {
    updateDraftAnswer(state, { answerIndex, value }) {
        state.creating.draft.answers[answerIndex].answerValue = value
    },
    updateDraftQuestion(state, value) {
        state.creating.draft.question = value
    },
    updateDraftResult(state, { answerIndex, resultValue, checkboxValue }) {
        state.creating.draft.answers[answerIndex].answerResult = checkboxValue
        state.creating.draft.result = resultValue
    },
    updateCreatingResult(state, value) {
        state.creating.result += value
    },
    updateCreatingScore(state, value) {
        state.creating.score = value
    },
    updateCreatingTitle(state, value) {
        state.creating.title = value
    },
    updateCreatingStatus(state, status) {
        state.creating.status = status
    },
    updateCreatingCurrentIndex(state) {
        state.creating.currentIndex = state.creating.list.length
    },
    addToCreatingList(state, item) {
        state.creating.list.push(item)
    },
    addToCreatingResult(state, value) {
        state.creating.result += value + '-'
    },
    resetCreating(state) {
        state.creating = JSON.parse(JSON.stringify(documentCreatingStateInitial))
    },
    resetDraft(state) {
        state.creating.draft = {
            question: '',
            answers: [
                {
                    answerIndex: '0',
                    answerValue: '',
                    answerResult: false
                },
                {
                    answerIndex: '1',
                    answerValue: '',
                    answerResult: false
                },
                {
                    answerIndex: '-',
                    answerValue: '',
                    unknown: ''
                }
            ],
            answers_formatted: '0-[-]1-[-]',
            result: ''
        }
    },
    createDocument(state, { document, result }) {
        console.log('Document for creating', document)
        console.log('Result for creating', result)
        state.list.data.unshift(document)
    },
    updateCKEValue(state, value) {
        state.cke.value = value
    },
    updateCKEKeyValue(state, keyValue) {
        state.cke.keyValue = keyValue
    },
    openCKE(state) {
        state.cke.isOpened = true
    },
    closeCKE(state) {
        state.cke.isOpened = false
    },
    openPlaying(state) {
        state.playing.isOpened = true
    },
    closePlaying(state) {
        state.playing.isOpened = false
    },
    formatDocumentForPlaying(state, { document }) {
        document.answers.forEach(answer => {
            answer.answers = []
            const { answers_formatted } = answer
            const splited = answers_formatted.split('[-]')
            splited.forEach((item, index) => {
                if (item) {
                    answer.answers.push({
                        answerIndex: item.split('-')[0],
                        answerValue: item.split('-')[1],
                        answerResult: false,
                    })
                }
            })
        })
        state.playing.data = document
    },
    updatePlayingResult(state, { questionIndex, result }) {
        state.playing.result[questionIndex] = { value: result }
        state.playing.result = { ...state.playing.result }
    },
    updatePlayingMarker(state, { questionIndex, marked }) {
        console.log('XXX', marked)
        state.playing.data.answers[questionIndex].isMarked = marked
        const { answers}  = state.playing.data
        state.playing.data.answers = { ...answers }
    },
    updateQuestionIndex(state, { index }) {
        state.playing.questionIndex = index
    },
    confirmedPlaying(state) {
        state.playing.status = 'CONFIRMED'
    },
    completedPlaying(state) {
        state.playing.status = 'COMPLETED'
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
