import { modalConfirmStateInitial } from './index';
// initial state
const state = () => ({
  locales: ['en', 'vi'],
  locale: 'en',
  menu: {
    status: 'menu-expanded',
    itemActive: ''
  },
  sidebar: {
    status: 'side-collapsed',
    itemActive: ''
  },
  modal: {
    status: 'modal-collapsed',
    component: null,
    mode: '',
    confirmedAt: ''
  },
  modalConfirm: { ...modalConfirmStateInitial },
  emoji: {
    isOpened: false,
    selected: null
  },
  datetime: {
    isOpened: false,
    value: null,
    mode: ''
  }
})

// getters
const getters = {

}

// actions
const actions = {
  updateMenuStatus ({ state, commit }, status) {
    if (status === 'OPEN') {
      commit('onUpdateMenuStatus', 'menu-expanded')
    }
    if (status === 'CLOSE') {
      commit('onUpdateMenuStatus', 'menu-collapsed')
    }
  },
  updateSidebar ({ state, commit }, item) {
    const status = item ? 'sidebar-expanded' : 'sidebar-collapsed'
    commit('onUpdateSidebar', { status, item })
  },
  updateSidebarMenu ({ state, commit }, data) {
    if (data.key === 'menu') {
      if (data.isMobile) {
        state.menu.status = 'menu-collapsed'
      }
    }
    if (data.key === 'sidebar') {
      if (data.itemActive) {
        if (state.sidebar.itemActive === data.itemActive) {
          if (state.sidebar.status === 'sidebar-expanded') {
            state.sidebar.status = 'sidebar-collapsed'
          }
          if (state.sidebar.status === 'sidebar-collapsed') {
            state.sidebar.status = 'sidebar-expanded'
          }
          state.sidebar.itemActive = null
        } else {
          state.sidebar.status = 'sidebar-expanded'
          state.sidebar.itemActive = data.itemActive
        }
      } else {
        state.sidebar = {
          status: 'sidebar-collapsed',
          itemActive: null
        }
      }
    }
  },
  updateModal ({ state, commit }, data) {
    if (data) {
      commit('onUpdateModal', {
        status: 'modal-expanded',
        ...data,
      })
      return
    }
    commit('onUpdateModal', {
      status: 'modal-collapsed',
      component: null,
      title: ''
    })
  },
  // OK
  openModal({ state, commit }, data) {
    commit('openModal', data)
  },
  // OK
  closeModal({ state, commit }) {
    commit('closeModal')
  },
  // OK
  openConfirmModal({ state, commit }, data) {
    if (data) {
      return commit('openModalConfirmByCustom', data)
    }
    commit('openModalConfirmByDefault')
  },
  cancelModalConfirm({ state, commit }) {
    commit('cancelModalConfirm')
    commit('closeModalConfirm')
  },
  confirmModalConfirm({ state, commit }) {
    commit('confirmModalConfirm')
    commit('closeModalConfirm')
  }
}

// mutations
const mutations = {
  onUpdateMenuStatus (state, data) {
    state.menu.status = data
  },
  onUpdateSidebar (state, data) {
    state.sidebar = {
      status: data.status,
      itemActive: data.item
    }
  },
  onUpdateModal (state, data) {
    state.modal = data
  },
  updateModalFooter(state, data) {
    state.modal = { ...state.modal, ...data }
  },
  openModalConfirmByDefault(state) {
    state.modalConfirm = {
      ...modalConfirmStateInitial,
      status: 'OPEN'
    }
  },
  openModalConfirmByCustom(state, data) {
    state.modal = {
      status: 'modal-expanded'
    }
    state.modalConfirm = {
      ...state.modalConfirm,
      ...data,
      status: 'OPEN'
    }
  },
  closeModalConfirm(state, data) {
    state.modalConfirm.status = ''
    if (state.modalConfirm.forceClose) {
      state.modal = {
        status: 'modal-collapsed',
        component: null,
        mode: ''
      }
    }
  },
  cancelModalConfirm(state) {
    const modalState = state.modal
    state.modal = {
      ...modalState,
      confirmedAt: 'CANCELED_AT:'+ new Date().getTime()
    }
  },
  confirmModalConfirm(state) {
    const modalState = state.modal
    state.modal = {
      ...modalState,
      confirmedAt: 'CONFIRMED_AT:' + new Date().getTime()
    }
  },
  openModal(state, data) {
    state.modal = {
      ...data,
      status: 'modal-expanded'
    }
  },
  closeModal(state) {
    state.modal = {
      status: 'modal-collapsed',
      component: null,
      title: '',
      mode: ''
    }
  },
  openPickerEmoji(state) {
    state.emoji.isOpened = true
  },
  closePickerEmoji(state) {
    state.emoji.isOpened = false
  },
  selectEmoji(state, data) {
    state.emoji.selected = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
