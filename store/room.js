import { roomCreatingStateInitial } from './index'

// initial state
const state = () => ({
    list: {
        data: [],
        totalPage: 0,
        currentPage: 0,
        isLoading: false,
        error: null
    },
    creating: {  ...roomCreatingStateInitial },
    modal: null
})

// getters
const getters = {

}

// actions
const actions = {
    updateCreatingValues({ state, commit }, data) {
        commit('updateCreatingValues', data)
    },
    createRoom({ state, commit }) {
        commit('createRoom', state.creating)
    },
    register({ state, commit }) {
        console.log('I am here already')
        commit('openModal')
    },
    resetRoomCreating({ state, commit }) {
        commit('resetRoomCreating')
    }
}

// mutations
const mutations = {
    updateCreatingValues(state, { key, value }) {
        if (key === 'idDisabled') {
            return state.creating[key] = !value
        }
        return state.creating[key] = value
    },
    createRoom(state, room) {
        console.log('createRoom', room)
    },
    resetRoomCreating(state) {
        state.creating = { ...roomCreatingStateInitial }
    },
    openModal(state) {
        state.modal = true
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
