export const strict = false

export const modalConfirmStateInitial = {
    status: '',
    component: null,
    title: 'Confirmation !',
    content: 'Are you sure to delete all ?',
    txtCancel: 'Cancel',
    txtConfirm: 'Okay',
}

export const roomCreatingStateInitial = {
    id: '101',
    name: '',
    mode: '',
    idDisabled: false,
    options: {
        icon: '/icons/search.png',
        label: 'Room Mode',
        subLabel: '',
        items: [
            {
                id: 'PUBLIC',
                label: 'Public',
                subLabel: 'Everyone can join without your acceptation',
                icon: 'https://www.facebook.com/rsrc.php/v3/yZ/r/IDAR_LcON7I.png'
            },
            {
                id: 'PRIVATE',
                label: 'Private',
                icon: 'https://www.facebook.com/rsrc.php/v3/yv/r/0sXdhIpI-vn.png',
                subLabel: 'Everyone can be join if you accept'
            }
        ]
    }
}

export const documentCreatingStateInitial = {
    list: [
        // {
        //     question: '',
        //     answers: [],
        //     answersFormatted: '0-[-]1-',
        //     result: '0-01-012',
        //     options: null
        // }
    ],
    draft: {
        question: '',
        answers: [
            {
                answerIndex: '0',
                answerValue: '',
                answerResult: false
            },
            {
                answerIndex: '1',
                answerValue: '',
                answerResult: false
            },
            {
                answerIndex: '-',
                answerValue: '',
                unknown: ''
            }
        ],
        answers_formatted: '0-[-]1-[-]',
        result: ''
    },
    id: Math.random(),
    title: 'Draft Title',
    result: '',
    score: 0,
    currentIndex: 0,
    isSaved: false,
    isEdited: false,
    status: 'CREATE'
}