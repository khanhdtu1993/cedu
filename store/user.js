import { Auth } from 'aws-amplify';
import { getItem, setItem, removeItem } from '~/plugins/storage';

const state = () => ({
    attributes: getItem('User'),
    isLoading: getItem('isVerifying') ? true : false,
    error: null,
})

const getters = {}

const actions = {
    async currentUser({ state }) {
        try {
            state.isLoading = true
            const user = await Auth.currentAuthenticatedUser()
            state.attributes = user.attributes
            state.isLoading = false
            setItem('User', user.attributes)
            removeItem('isVerifying')
        } catch (error) {
            state.error = error
            state.isLoading = false
            state.attributes = null
            removeItem('isVerifying')
        }
    },
    loggout ({ state }) {
        Auth.signOut({ global: true })
        state.isLoading = true
        setItem('isVerifying', new Date())
        removeItem('User')
    }
}

const mutations = {
    loading (state, isLoading) {
        state.isLoading = isLoading
        setItem('isVerifying', new Date())
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}