/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUserContainer = /* GraphQL */ `
  subscription OnCreateUserContainer {
    onCreateUserContainer {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUserContainer = /* GraphQL */ `
  subscription OnUpdateUserContainer {
    onUpdateUserContainer {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUserContainer = /* GraphQL */ `
  subscription OnDeleteUserContainer {
    onDeleteUserContainer {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePage = /* GraphQL */ `
  subscription OnCreatePage {
    onCreatePage {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePage = /* GraphQL */ `
  subscription OnUpdatePage {
    onUpdatePage {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePage = /* GraphQL */ `
  subscription OnDeletePage {
    onDeletePage {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePageMember = /* GraphQL */ `
  subscription OnCreatePageMember {
    onCreatePageMember {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePageMember = /* GraphQL */ `
  subscription OnUpdatePageMember {
    onUpdatePageMember {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePageMember = /* GraphQL */ `
  subscription OnDeletePageMember {
    onDeletePageMember {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDocument = /* GraphQL */ `
  subscription OnCreateDocument {
    onCreateDocument {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDocument = /* GraphQL */ `
  subscription OnUpdateDocument {
    onUpdateDocument {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDocument = /* GraphQL */ `
  subscription OnDeleteDocument {
    onDeleteDocument {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDocumentContent = /* GraphQL */ `
  subscription OnCreateDocumentContent {
    onCreateDocumentContent {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDocumentContent = /* GraphQL */ `
  subscription OnUpdateDocumentContent {
    onUpdateDocumentContent {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDocumentContent = /* GraphQL */ `
  subscription OnDeleteDocumentContent {
    onDeleteDocumentContent {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDocumentInPage = /* GraphQL */ `
  subscription OnCreateDocumentInPage {
    onCreateDocumentInPage {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDocumentInPage = /* GraphQL */ `
  subscription OnUpdateDocumentInPage {
    onUpdateDocumentInPage {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDocumentInPage = /* GraphQL */ `
  subscription OnDeleteDocumentInPage {
    onDeleteDocumentInPage {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUserInDocument = /* GraphQL */ `
  subscription OnCreateUserInDocument {
    onCreateUserInDocument {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUserInDocument = /* GraphQL */ `
  subscription OnUpdateUserInDocument {
    onUpdateUserInDocument {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUserInDocument = /* GraphQL */ `
  subscription OnDeleteUserInDocument {
    onDeleteUserInDocument {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDocumentAnswers = /* GraphQL */ `
  subscription OnCreateDocumentAnswers {
    onCreateDocumentAnswers {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDocumentAnswers = /* GraphQL */ `
  subscription OnUpdateDocumentAnswers {
    onUpdateDocumentAnswers {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDocumentAnswers = /* GraphQL */ `
  subscription OnDeleteDocumentAnswers {
    onDeleteDocumentAnswers {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePost = /* GraphQL */ `
  subscription OnCreatePost {
    onCreatePost {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePost = /* GraphQL */ `
  subscription OnUpdatePost {
    onUpdatePost {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePost = /* GraphQL */ `
  subscription OnDeletePost {
    onDeletePost {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateComment = /* GraphQL */ `
  subscription OnCreateComment {
    onCreateComment {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateComment = /* GraphQL */ `
  subscription OnUpdateComment {
    onUpdateComment {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteComment = /* GraphQL */ `
  subscription OnDeleteComment {
    onDeleteComment {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateLike = /* GraphQL */ `
  subscription OnCreateLike {
    onCreateLike {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateLike = /* GraphQL */ `
  subscription OnUpdateLike {
    onUpdateLike {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteLike = /* GraphQL */ `
  subscription OnDeleteLike {
    onDeleteLike {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSchedule = /* GraphQL */ `
  subscription OnCreateSchedule {
    onCreateSchedule {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSchedule = /* GraphQL */ `
  subscription OnUpdateSchedule {
    onUpdateSchedule {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSchedule = /* GraphQL */ `
  subscription OnDeleteSchedule {
    onDeleteSchedule {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
