/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUserContainer = /* GraphQL */ `
  mutation CreateUserContainer(
    $input: CreateUserContainerInput!
    $condition: ModelUserContainerConditionInput
  ) {
    createUserContainer(input: $input, condition: $condition) {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUserContainer = /* GraphQL */ `
  mutation UpdateUserContainer(
    $input: UpdateUserContainerInput!
    $condition: ModelUserContainerConditionInput
  ) {
    updateUserContainer(input: $input, condition: $condition) {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUserContainer = /* GraphQL */ `
  mutation DeleteUserContainer(
    $input: DeleteUserContainerInput!
    $condition: ModelUserContainerConditionInput
  ) {
    deleteUserContainer(input: $input, condition: $condition) {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createPage = /* GraphQL */ `
  mutation CreatePage(
    $input: CreatePageInput!
    $condition: ModelPageConditionInput
  ) {
    createPage(input: $input, condition: $condition) {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updatePage = /* GraphQL */ `
  mutation UpdatePage(
    $input: UpdatePageInput!
    $condition: ModelPageConditionInput
  ) {
    updatePage(input: $input, condition: $condition) {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deletePage = /* GraphQL */ `
  mutation DeletePage(
    $input: DeletePageInput!
    $condition: ModelPageConditionInput
  ) {
    deletePage(input: $input, condition: $condition) {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createPageMember = /* GraphQL */ `
  mutation CreatePageMember(
    $input: CreatePageMemberInput!
    $condition: ModelPageMemberConditionInput
  ) {
    createPageMember(input: $input, condition: $condition) {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updatePageMember = /* GraphQL */ `
  mutation UpdatePageMember(
    $input: UpdatePageMemberInput!
    $condition: ModelPageMemberConditionInput
  ) {
    updatePageMember(input: $input, condition: $condition) {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deletePageMember = /* GraphQL */ `
  mutation DeletePageMember(
    $input: DeletePageMemberInput!
    $condition: ModelPageMemberConditionInput
  ) {
    deletePageMember(input: $input, condition: $condition) {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createDocument = /* GraphQL */ `
  mutation CreateDocument(
    $input: CreateDocumentInput!
    $condition: ModelDocumentConditionInput
  ) {
    createDocument(input: $input, condition: $condition) {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDocument = /* GraphQL */ `
  mutation UpdateDocument(
    $input: UpdateDocumentInput!
    $condition: ModelDocumentConditionInput
  ) {
    updateDocument(input: $input, condition: $condition) {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDocument = /* GraphQL */ `
  mutation DeleteDocument(
    $input: DeleteDocumentInput!
    $condition: ModelDocumentConditionInput
  ) {
    deleteDocument(input: $input, condition: $condition) {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createDocumentContent = /* GraphQL */ `
  mutation CreateDocumentContent(
    $input: CreateDocumentContentInput!
    $condition: ModelDocumentContentConditionInput
  ) {
    createDocumentContent(input: $input, condition: $condition) {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDocumentContent = /* GraphQL */ `
  mutation UpdateDocumentContent(
    $input: UpdateDocumentContentInput!
    $condition: ModelDocumentContentConditionInput
  ) {
    updateDocumentContent(input: $input, condition: $condition) {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDocumentContent = /* GraphQL */ `
  mutation DeleteDocumentContent(
    $input: DeleteDocumentContentInput!
    $condition: ModelDocumentContentConditionInput
  ) {
    deleteDocumentContent(input: $input, condition: $condition) {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const createDocumentInPage = /* GraphQL */ `
  mutation CreateDocumentInPage(
    $input: CreateDocumentInPageInput!
    $condition: ModelDocumentInPageConditionInput
  ) {
    createDocumentInPage(input: $input, condition: $condition) {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDocumentInPage = /* GraphQL */ `
  mutation UpdateDocumentInPage(
    $input: UpdateDocumentInPageInput!
    $condition: ModelDocumentInPageConditionInput
  ) {
    updateDocumentInPage(input: $input, condition: $condition) {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDocumentInPage = /* GraphQL */ `
  mutation DeleteDocumentInPage(
    $input: DeleteDocumentInPageInput!
    $condition: ModelDocumentInPageConditionInput
  ) {
    deleteDocumentInPage(input: $input, condition: $condition) {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createUserInDocument = /* GraphQL */ `
  mutation CreateUserInDocument(
    $input: CreateUserInDocumentInput!
    $condition: ModelUserInDocumentConditionInput
  ) {
    createUserInDocument(input: $input, condition: $condition) {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const updateUserInDocument = /* GraphQL */ `
  mutation UpdateUserInDocument(
    $input: UpdateUserInDocumentInput!
    $condition: ModelUserInDocumentConditionInput
  ) {
    updateUserInDocument(input: $input, condition: $condition) {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const deleteUserInDocument = /* GraphQL */ `
  mutation DeleteUserInDocument(
    $input: DeleteUserInDocumentInput!
    $condition: ModelUserInDocumentConditionInput
  ) {
    deleteUserInDocument(input: $input, condition: $condition) {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const createDocumentAnswers = /* GraphQL */ `
  mutation CreateDocumentAnswers(
    $input: CreateDocumentAnswersInput!
    $condition: ModelDocumentAnswersConditionInput
  ) {
    createDocumentAnswers(input: $input, condition: $condition) {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const updateDocumentAnswers = /* GraphQL */ `
  mutation UpdateDocumentAnswers(
    $input: UpdateDocumentAnswersInput!
    $condition: ModelDocumentAnswersConditionInput
  ) {
    updateDocumentAnswers(input: $input, condition: $condition) {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const deleteDocumentAnswers = /* GraphQL */ `
  mutation DeleteDocumentAnswers(
    $input: DeleteDocumentAnswersInput!
    $condition: ModelDocumentAnswersConditionInput
  ) {
    deleteDocumentAnswers(input: $input, condition: $condition) {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const createPost = /* GraphQL */ `
  mutation CreatePost(
    $input: CreatePostInput!
    $condition: ModelPostConditionInput
  ) {
    createPost(input: $input, condition: $condition) {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updatePost = /* GraphQL */ `
  mutation UpdatePost(
    $input: UpdatePostInput!
    $condition: ModelPostConditionInput
  ) {
    updatePost(input: $input, condition: $condition) {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deletePost = /* GraphQL */ `
  mutation DeletePost(
    $input: DeletePostInput!
    $condition: ModelPostConditionInput
  ) {
    deletePost(input: $input, condition: $condition) {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createComment = /* GraphQL */ `
  mutation CreateComment(
    $input: CreateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    createComment(input: $input, condition: $condition) {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateComment = /* GraphQL */ `
  mutation UpdateComment(
    $input: UpdateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    updateComment(input: $input, condition: $condition) {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteComment = /* GraphQL */ `
  mutation DeleteComment(
    $input: DeleteCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    deleteComment(input: $input, condition: $condition) {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLike = /* GraphQL */ `
  mutation CreateLike(
    $input: CreateLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    createLike(input: $input, condition: $condition) {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLike = /* GraphQL */ `
  mutation UpdateLike(
    $input: UpdateLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    updateLike(input: $input, condition: $condition) {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLike = /* GraphQL */ `
  mutation DeleteLike(
    $input: DeleteLikeInput!
    $condition: ModelLikeConditionInput
  ) {
    deleteLike(input: $input, condition: $condition) {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createSchedule = /* GraphQL */ `
  mutation CreateSchedule(
    $input: CreateScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    createSchedule(input: $input, condition: $condition) {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
export const updateSchedule = /* GraphQL */ `
  mutation UpdateSchedule(
    $input: UpdateScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    updateSchedule(input: $input, condition: $condition) {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
export const deleteSchedule = /* GraphQL */ `
  mutation DeleteSchedule(
    $input: DeleteScheduleInput!
    $condition: ModelScheduleConditionInput
  ) {
    deleteSchedule(input: $input, condition: $condition) {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
