/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUserContainer = /* GraphQL */ `
  query GetUserContainer($id: ID!) {
    getUserContainer(id: $id) {
      id
      user_mode
      fullname
      avatar
      online_status
      location {
        lat
        lng
      }
      pages {
        nextToken
      }
      documents {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUserContainers = /* GraphQL */ `
  query ListUserContainers(
    $filter: ModelUserContainerFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserContainers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPage = /* GraphQL */ `
  query GetPage($id: ID!) {
    getPage(id: $id) {
      id
      page_key
      page_name
      page_mode
      page_type
      current_members_count
      limit_members_count
      schedule {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listPages = /* GraphQL */ `
  query ListPages(
    $filter: ModelPageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        page_key
        page_name
        page_mode
        page_type
        current_members_count
        limit_members_count
        datetime_created
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPageMember = /* GraphQL */ `
  query GetPageMember($id: ID!) {
    getPageMember(id: $id) {
      id
      page_id
      scores_count
      is_confirmed
      _user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listPageMembers = /* GraphQL */ `
  query ListPageMembers(
    $filter: ModelPageMemberFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPageMembers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        page_id
        scores_count
        is_confirmed
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocument = /* GraphQL */ `
  query GetDocument($id: ID!) {
    getDocument(id: $id) {
      id
      title
      type
      template
      content {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      durationTime
      expiredTime
      datetime_created
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDocuments = /* GraphQL */ `
  query ListDocuments(
    $filter: ModelDocumentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentContent = /* GraphQL */ `
  query GetDocumentContent($id: ID!) {
    getDocumentContent(id: $id) {
      id
      document_id
      index
      question
      answers
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDocumentContents = /* GraphQL */ `
  query ListDocumentContents(
    $filter: ModelDocumentContentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDocumentContents(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentInPage = /* GraphQL */ `
  query GetDocumentInPage($id: ID!) {
    getDocumentInPage(id: $id) {
      id
      page_id
      document {
        id
        title
        type
        template
        durationTime
        expiredTime
        datetime_created
        createdAt
        updatedAt
      }
      users_played {
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDocumentInPages = /* GraphQL */ `
  query ListDocumentInPages(
    $filter: ModelDocumentInPageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDocumentInPages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        page_id
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUserInDocument = /* GraphQL */ `
  query GetUserInDocument($id: ID!) {
    getUserInDocument(id: $id) {
      id
      document_in_page {
        id
        page_id
        createdAt
        updatedAt
      }
      user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      answers
      score
      createdAt
      updatedAt
    }
  }
`;
export const listUserInDocuments = /* GraphQL */ `
  query ListUserInDocuments(
    $filter: ModelUserInDocumentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserInDocuments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        answers
        score
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentAnswers = /* GraphQL */ `
  query GetDocumentAnswers($id: ID!) {
    getDocumentAnswers(id: $id) {
      id
      document_id
      answers
      createdAt
      updatedAt
    }
  }
`;
export const listDocumentAnswerss = /* GraphQL */ `
  query ListDocumentAnswerss(
    $filter: ModelDocumentAnswersFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDocumentAnswerss(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        document_id
        answers
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPost = /* GraphQL */ `
  query GetPost($id: ID!) {
    getPost(id: $id) {
      id
      page_id
      content
      options {
        image
        video
        audio
        score
        seconds
      }
      likes {
        nextToken
      }
      comments {
        nextToken
      }
      _ref_user_created {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listPosts = /* GraphQL */ `
  query ListPosts(
    $filter: ModelPostFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getComment = /* GraphQL */ `
  query GetComment($id: ID!) {
    getComment(id: $id) {
      id
      comment
      datetime_created
      _ref_post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listComments = /* GraphQL */ `
  query ListComments(
    $filter: ModelCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        comment
        datetime_created
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLike = /* GraphQL */ `
  query GetLike($postId_userId: ID!) {
    getLike(postId_userId: $postId_userId) {
      postId_userId
      post {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      _ref_user {
        id
        user_mode
        fullname
        avatar
        online_status
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLikes = /* GraphQL */ `
  query ListLikes(
    $postId_userId: ID
    $filter: ModelLikeFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listLikes(
      postId_userId: $postId_userId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        postId_userId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSchedule = /* GraphQL */ `
  query GetSchedule($id: ID!) {
    getSchedule(id: $id) {
      id
      datetime_created
      datetime_expired
      datetime_start
      datetime_finish
      day_of_week
      day_of_year
      month_of_year
      createdAt
      updatedAt
    }
  }
`;
export const listSchedules = /* GraphQL */ `
  query ListSchedules(
    $filter: ModelScheduleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSchedules(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        datetime_created
        datetime_expired
        datetime_start
        datetime_finish
        day_of_week
        day_of_year
        month_of_year
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const listPublicPages = /* GraphQL */ `
  query ListPublicPages(
    $page_type: PageTypeCategory
    $page_mode: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelPageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPublicPages(
      page_type: $page_type
      page_mode: $page_mode
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        page_key
        page_name
        page_mode
        page_type
        current_members_count
        limit_members_count
        datetime_created
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPageMembersByPID = /* GraphQL */ `
  query GetPageMembersByPID(
    $page_id: String
    $sortDirection: ModelSortDirection
    $filter: ModelPageMemberFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getPageMembersByPID(
      page_id: $page_id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        page_id
        scores_count
        is_confirmed
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentContentByDID = /* GraphQL */ `
  query GetDocumentContentByDID(
    $document_id: String
    $sortDirection: ModelSortDirection
    $filter: ModelDocumentContentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDocumentContentByDID(
      document_id: $document_id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        document_id
        index
        question
        answers
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentsInPageByDID = /* GraphQL */ `
  query GetDocumentsInPageByDID(
    $page_id: String
    $sortDirection: ModelSortDirection
    $filter: ModelDocumentInPageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDocumentsInPageByDID(
      page_id: $page_id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        page_id
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDocumentAnswersByDID = /* GraphQL */ `
  query GetDocumentAnswersByDID(
    $document_id: String
    $sortDirection: ModelSortDirection
    $filter: ModelDocumentAnswersFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getDocumentAnswersByDID(
      document_id: $document_id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        document_id
        answers
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPostsInPageByPID = /* GraphQL */ `
  query GetPostsInPageByPID(
    $page_id: String
    $sortDirection: ModelSortDirection
    $filter: ModelPostFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getPostsInPageByPID(
      page_id: $page_id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        page_id
        content
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
