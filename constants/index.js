export const ICON_ACTIONS = {
    REGISTER : '[ICON]-REGISTER',
    JOIN : '[ICON]-JOIN',
    VIEW : '[ICON]-VIEW',
    EDIT : '[ICON]-EDIT',
    DELETE : '[ICON]-DELETE',
    CANCEL : '[ICON]-CANCEL',
};

export const ICON_STATUS = {
    INPROGRESS: 'In-Progress',
    OPENING: 'Opening',
    FINISHED: 'Finished',
}