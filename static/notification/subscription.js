import { API, graphqlOperation } from 'aws-amplify'
import { onCreatePost } from '~/graphql/subscriptions'

addEventListener('message', (event) => {
    try {
        console.log('[Service Worker] Push Received.')
        console.log(`[Service Worker] Push had this data: "${event.data}"`)
        const parser = event.data ? JSON.parse(event.data) : event.data
        // if (parser && parser.action === 'NEW_POSTS') {
        API.graphql(graphqlOperation(onCreatePost))
        .subscribe(newPost => {
            (console.log('newPost', newPost)),
            (error => console.log('Error new Post', error))
        })
        // }
    } catch (error) {
        console.warn('Socket is disconected !')
    }
});


addEventListener('active', (event) => {
    console.log('[Service Worker] Active Event', event)
})