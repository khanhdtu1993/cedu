var appCacheFiles = [
    '/',
    '/index.html'
],

appCache = 'aws-amplify-v1'

addEventListener('install', (event) => {
    console.log('[Service Worker] Install Event', event)
    event.waitUntil(
        caches.open(appCache).then(function(cache) {
            return cache.addAll(appCacheFiles);
        })
    );
})

addEventListener('active', (event) => {
    console.log('[Service Worker] Active Event', event)
})

addEventListener('push', (event) => {
    let data = {};
    console.log('[Service Worker] Push Received.');
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
});
